@extends('base', ['meta_title'=>'Index'])

@section('content')
<hr></hr>

@foreach($comments as $comment)
	<div class="well well-sm"><b> {{$comment->name}} ( {{ $comment->ip }} )</b> {{ $comment->date }}</br>
	{{ $comment->comment }}</br>
	<a href ="{{ url('/admin/reply/' . $comment->id)}}">Atsakyti</a>|<a href ="{{ url('/admin/delete/' . $comment->id)}}">Trinti</a>
	</div>
	@foreach($answers as $answer)		
		@if($answer->comment_id == $comment->id)
			<div class="row">
				<div class="col-md-11 col-md-offset-1">
					<div class="well well-sm"><b>{{$answer->name}} ( {{$answer->ip}} )</b> {{$answer->date}}</br>
					{{$answer->comment}}</br></div>
				</div>
			</div>
		@endif
	@endforeach	
@endforeach

@stop