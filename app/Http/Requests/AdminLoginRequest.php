<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;

class AdminLoginRequest extends Request{
    
	public function authorize(){
		return true;
	}
	
	public function rules(){
		return[
			'username' => 'required',
			'password' => 'required'
		];
	}
	
	public function messages(){
		return [
			'username.required' => 'Neivestas vardas',
			'password.required' => 'Neivestas slaptazodis'
		];
	}
}
