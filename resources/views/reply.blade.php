@extends('base', ['meta_title'=>'Index'])

@section('content')
<div class="row">
	<div class="col-md-4 col-md-offset-4">
	@if($errors->has())
		@foreach($errors->all() as $error)
		{{ $error }}<br/>
		@endforeach
	<br/>
	@endif	
	{!! Form::open(array('url'=>'admin/reply/result/'.$id)) !!}
		<div class="form-group">
		{!! Form::label('atsakymas','Atsakymas') !!}	
		{!! Form::textarea('atsakymas', '', array('class'=>'form-control','placeholder'=>'Hi')) !!}
		</div>
		{!! Form::submit('Atsakyti', array('class'=>'btn btn-default')) !!}
	{!! Form::close() !!}	
	</div>
</div>

@stop