<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Answer;
use App\Http\Controllers\Controller;
use App\Http\Requests\AdminLoginRequest;
use App\Http\Requests\ReplyRequest;
use Auth;
use App\Http\Controllers\Input;

class AdminController extends Controller{
    
   public function login(AdminLoginRequest $request){
   		echo "sd";
   		if(Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('password')])){
   			return redirect("/admin");
   		}else{
   			return redirect('/admin/login');
   		}
   }

   public function cpanel(){
   		if(Auth::check()){
   			$comments = Comment::orderBy('id','DESC')->limit(10)->get();
			$answers = Answer::all();

			return view('admin', ['comments' => $comments, 'answers' => $answers]);
   		}else{
   			return redirect('admin/login');
   		}
   }

   public function reply ($id){
		if(Auth::check()){
   			return view('reply', ['id'=>$id]);
   		}else{
   			return redirect('admin/login');
   		}
   }

   public function replyResult($id, ReplyRequest $request){
   		if(Auth::check()){
   			$answer = new Answer();
   			$answer->comment_id = $id;
   			$answer->name = 'Administratorius';
			$answer->comment = $request->input('atsakymas');
			$answer->ip = $_SERVER['REMOTE_ADDR'];
			$answer->date = date('d-m-Y H:i:s');
			$answer->save();
   			return redirect('admin/');
   		}else{
   			return redirect('admin/login');
   		}
   }

   public function delete($id){
   		if(Auth::check()){
   			$comment = Comment::find($id);
   			$comment->delete();
   			return redirect('admin');
   		}else{
   			return redirect('admin/login');
   		}
   }
}
