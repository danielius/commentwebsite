@extends('base', ['meta_title'=>'Index'])

@section('content')
<div class="row">
	<div class="col-md-4 col-md-offset-4">
	@if($errors->has())
		@foreach($errors->all() as $error)
		{{ $error }}<br/>
		@endforeach
	<br/>
	@endif	
	{!! Form::open(array('url'=>'/admin')) !!}
		<div class="form-group">
		{!! Form::label('username', 'Username') !!}
		{!! Form::text('username', '', array('class'=>'form-control')) !!}
		</div>
		<div class="form-group">
		{!! Form::label('password','Password') !!}	
		{!! Form::text('password', '', array('class'=>'form-control')) !!}
		</div>
		{!! Form::submit('Jungtis', array('class'=>'btn btn-default')) !!}
	{!! Form::close() !!}	
	</div>
</div>

@stop