<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Answer;
use App\Http\Controllers\Controller;
use App\Http\Requests\WriteCommentRequest;

class CommentController extends Controller{
   
   public function showComments(){
	    // controller paima is Comment Model klases per Eloquent
		$comments = Comment::orderBy('id','DESC')->limit(10)->get();
		$answers = Answer::all();

		return view('chat', ['comments' => $comments, 'answers' => $answers]);
	}
	
	public function storeComment(WriteCommentRequest $request){		
		$ip = $_SERVER['REMOTE_ADDR'];
		$comment = new Comment();
		$comment->name = $request->input('vardas');
		$comment->comment = $request->input('komentaras');
		$comment->ip = $_SERVER['REMOTE_ADDR'];
		$comment->date = date('d-m-Y H:i:s');
		$comment->save();
		
		return redirect('/');
	}
}
