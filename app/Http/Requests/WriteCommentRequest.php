<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;

class WriteCommentRequest extends Request{
    
	public function authorize(){
		return true;
	}
	
	public function rules(){
		return[
			'vardas' => 'required',
			'komentaras' => 'required'
		];
	}
	
	public function messages(){
		return [
			'vardas.required' => 'Neivestas vardas',
			'komentaras.required' => 'Neivestas komentaras'
		];
	}
}
