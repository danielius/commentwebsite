<?php
namespace App\Http\Requests;

use App\Http\Requests\Request;

class ReplyRequest extends Request{
    
	public function authorize(){
		return true;
	}
	
	public function rules(){
		return[
			'atsakymas' => 'required'
		];
	}
	
	public function messages(){
		return [
			'atsakymas.required' => 'Neivestas atsakymas'
		];
	}
}
