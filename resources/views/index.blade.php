@extends('base', ['meta_title'=>'Index'])

@section('content')
<a href ="{{ url('/chat')}}">Chat</a></br>
<a href="{{ url('/login')}}">Prisijungti</a>
<a href="{{ url('/register')}}">Registruotis</a>
<hr></hr>

@foreach($comments as $comment)
	<div class="well well-sm"><b> {{$comment->name}} ( {{ $comment->ip }} )</b> {{ $comment->date }}</br>
	{{ $comment->comment }}</br>
	</div>
	@foreach($answers as $answer)		
		@if($answer->comment_id == $comment->id)
			<div class="row">
				<div class="col-md-11 col-md-offset-1">
					<div class="well well-sm"><b>{{$answer->name}} ( {{$answer->ip}} )</b> {{$answer->date}}</br>
					{{$answer->comment}}</br></div>
				</div>
			</div>
		@endif
	@endforeach	
@endforeach

@stop