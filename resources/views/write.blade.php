@extends('base', ['meta_title'=>'Index'])

@section('content')
<div class="row">
	<div class="col-md-4 col-md-offset-4">
	@if($errors->has())
		@foreach($errors->all() as $error)
		{{ $error }}<br/>
		@endforeach
	<br/>
	@endif	
	{!! Form::open(array('url'=>'write/result')) !!}
		<div class="form-group">
		{!! Form::label('vardas', 'Vardas') !!}
		{!! Form::text('vardas', '', array('class'=>'form-control','placeholder'=>'John')) !!}
		</div>
		<div class="form-group">
		{!! Form::label('komentaras','Komentaras') !!}	
		{!! Form::textarea('komentaras', '', array('class'=>'form-control','placeholder'=>'Hi')) !!}
		</div>
		{!! Form::submit('Rasyti', array('class'=>'btn btn-default')) !!}
	{!! Form::close() !!}	
	</div>
</div>

@stop