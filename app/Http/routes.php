<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/',function(){
	return view('index');.
});

Route::get('/chat', 'CommentController@showComments');

Route::get('/write',function(){
	return view('write');
});

Route::post('write/result', 'CommentController@storeComment');

Route::get('/login',function(){
	return view('login');
});

Route::post('/admin', 'AdminController@login');
Route::get('/admin', 'AdminController@cpanel');
Route::get('/admin/reply/{id}', 'AdminController@reply');
Route::post('/admin/reply/result/{id}', 'AdminController@replyResult');
Route::get('/admin/delete/{id}', 'AdminController@delete');